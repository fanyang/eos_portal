import * as THREE from 'three'
import { is_mobile } from '../utils/index.js'
import $ from 'jquery'
import {TweenMax, Power3, Power4, TimelineLite} from "gsap/TweenMax"
import 'three/examples/js/renderers/Projector.js'
import 'three/examples/js/renderers/CanvasRenderer'
import draw_img from '../../assets/images/particle.png'
import displace_map from '../../assets/images/displace-map.jpg'

var a = new THREE.Scene;
let camera = new THREE.PerspectiveCamera(45*(is_mobile() ? 1.5 : 1), window.innerWidth / window.innerHeight, .1, 1e3);
var t = new THREE.WebGLRenderer({
    alpha: !0
});
    t.setClearColor(0, 0), 
    t.setSize(window.innerWidth, window.innerHeight), 
    t.shadowMapEnabled = !0, 
    $(window).on("resize", function() {
        t.setSize(window.innerWidth, window.innerHeight), 
        camera.aspect = window.innerWidth / window.innerHeight, camera.updateProjectionMatrix()
    }), 
    camera.position.x = -60, 
    camera.position.y = 60, 
    camera.position.z = 60, 
    camera.lookAt(new THREE.Vector3(0, 0, 0)), $("#container").append(t.domElement);
    var r, e, i , gr = 0;
    var controls;
    controls = new function() {
        this.radius = 1, 
        this.tube = 10, 
        this.radialSegments = 350, 
        // isMob() ? this.radialSegments = 100 : this.radialSegments = 350, 
        this.radialSegments = 350, 
        this.tubularSegments = 12, 
        this.p = 5, 
        this.q = 4, 
        this.heightScale = 4, 
        this.asParticles = !0, 
        this.rotate = !0;
        Power3.easeInOut;

        this.redraw = function(color) {
            r && a.remove(r);
            var e, t, i, n, o, s = new THREE.TorusKnotGeometry(
                controls.radius, 
                controls.tube, 
                Math.round(controls.radialSegments), 
                Math.round(controls.tubularSegments), 
                Math.round(controls.p), 
                Math.round(controls.q), 
                controls.heightScale
            );
            controls.asParticles ? (i = s, n = new THREE.ParticleBasicMaterial({
                color: '16ffffff',
                size: .5,
                transparent: !0,
                blending: THREE.AdditiveBlending,
                map: function() {
                    var e = document.createElement("canvas");
                    e.width = 16, e.height = 16;
                    var t = e.getContext("2d"),
                        i = t.createRadialGradient(e.width / 2, e.height / 2, 0, e.width / 2, e.height / 2, e.width / 2);
                    // i.addColorStop(1, "rgba(255,255,255,0.4)");
                    i.addColorStop(1, color || "rgba(0, 159, 224, 0.8)");
                    i.addColorStop(1, "rgba(0,0,0,0)");
                    t.fillStyle = i, 
                    t.fillRect(0, 0, e.width, e.height);
                    var n = new THREE.Texture(e);
                    return n.needsUpdate = !0, n
                }()
            }), 
            (o = new THREE.ParticleSystem(i, n))
            .sortParticles = !0, r = o) : (e = s, (t = new THREE.MeshNormalMaterial({})).side = THREE.DoubleSide, r = THREE.SceneUtils.createMultiMaterialObject(e, [t])), 
            a.add(r)
        }, 
        this.start = function() {
            controls.tube;
            if (controls.radius = 1, false)(new TimelineMax).to(controls, 3, {
                tube: 30,
                ease: Power4.easeOut,
                onUpdate: function() {
                    controls.redraw()
                }
            });
            else {

                    (function() {
                        controls.redraw();
                    })();
                (new TimelineMax).to(controls, 3, {
                    tube: 35,
                    ease: Power4.easeOut,
                    onUpdate: function() {
                        controls.redraw()
                    }
                }).staggerTo(".main__header .letter", 1, {
                    x: "0%",
                    opacity: 1
                }, .05, "-=2.4").staggerTo(".main__year .letter", 1, {
                    x: "0%",
                    opacity: 1,
                    onComplete: function() {
                        $(".main__year, .prize-header").addClass("header-glitch")
                    }
                }, .05, "-=2").to(".main__desc", .8, {
                    ease: Power1.easeInOut,
                    y: "0%",
                    opacity: 1
                }, "-=1.8").to(".main__btn", .8, {
                    ease: Power1.easeInOut,
                    y: "0%",
                    opacity: 1
                }, "-=1.4").fromTo(".header", 1.4, {
                    y: "-100%",
                    opacity: 0
                }, {
                    y: "0%",
                    opacity: 1,
                    ease: Power2.easeInOut
                }, "-=1.8").to(".fixed-nav", 1.4, {
                    x: "0%",
                    opacity: 1,
                    ease: Power2.easeInOut
                }, "-=1.6").fromTo(".section-main .hud-bottom", .6, {
                    y: "100%",
                    opacity: 0
                }, {
                    y: "0%",
                    opacity: 1
                }, "-=0.9").fromTo(".section-main .hud-timer", .6, {
                    y: "200%",
                    opacity: 0
                }, {
                    y: "120%",
                    opacity: 1
                }, "-=0.8")
            }
        }, 
        this.prestart = function() {
            TweenMax.to(controls, 1.8, {
                radius: 1,
                ease: Power2.easeOut,
                onUpdate: function() {
                    controls.redraw()
                }
            });
        }, 
        this.prestartReverse = function() {
            TweenMax.to(controls, 1.8, {
                radius: 20,
                ease: Power2.easeOut,
                onUpdate: function() {
                    controls.redraw()
                }
            })
        }
    }
    
    var i = 0;
    controls.start();
    controls.prestart();
    controls.rend = function e() {
        controls.rotate && (r.rotation.y = i += .001);
        TweenMax.ticker.addEventListener("tick", e);
        t.render(a, camera);
    };
    controls.rend();

let is_expand = false;
export const expand = () => {
    if(is_expand){ return ;}
    is_expand = true;
    TweenMax.to(controls, 30, {
        radius: 60,
        ease: Power2.easeOut,
        // duration: 1000,
        onUpdate: function() {
            controls.redraw("rgba(0, 159, 224, 0.2)")
        }
    });
}

export const contract = () => {
    if(!is_expand){ return ;}
    is_expand = false;
    TweenMax.to(controls, 1, {
        radius: 1,
        ease: Power2.easeOut,
        onUpdate: function() {
            controls.redraw()
        }
    });
}

import * as PIXI from 'pixi.js'
import regeneratorRuntime from "regenerator-runtime"

(function(){
    var i = function() {
        function n(e, t) {
            for (var i = 0; i < t.length; i++) {
                var n = t[i];
                n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n)
            }
        }
        return function(e, t, i) {
            return t && n(e.prototype, t), i && n(e, i), e
        }
    }();
    function s(e) {
        return function() {
            var r = e.apply(this, arguments);
            return new Promise(function(s, a) {
                return function t(e, i) {
                    try {
                        var n = r[e](i),
                            o = n.value
                    } catch (e) {
                        return void a(e)
                    }
                    if (!n.done) return Promise.resolve(o).then(function(e) {
                        t("next", e)
                    }, function(e) {
                        t("throw", e)
                    });
                    s(o)
                }("next")
            })
        }
    }
    function a(e, t) {
        if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return !t || "object" != typeof t && "function" != typeof t ? e : t
    }
var e = function(e) {
    function o(e) {
        var t, i = this;
        ! function(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }(this, o);
        var n = a(this, (o.__proto__ || Object.getPrototypeOf(o)).call(this, {
            autoStart: !1,
            autoResize: !0,
            transparent: !0
        }));
        return n.resize = o.debounce(s(regeneratorRuntime.mark(function e() {
            return regeneratorRuntime.wrap(function(e) {
                for (;;) switch (e.prev = e.next) {
                    case 0:
                        n.setSize() && (n.container.removeChildren(0, n.container.children.length - 1), n.addGraphics(), n.createTimeLine(), n.render());
                    case 1:
                    case "end":
                        return e.stop()
                }
            }, e, i)
        })), n, 100), n.options = Object.assign({}, {
            backgroundColor: 4748748,
            backgroundAlpha: 1,
            polygon: "0, 0, 0, 0",
            borderColor: 4748748,
            borderWidth: 0,
            wavesAlpha: 1,
            displacementScale: {
                x: 10,
                y: 10
            },
            displacementMap: "../img/displace-map.jpg"
        }, e), n.polygon = n.options.polygon.replace(/\s/g, "").split(",").map(function(e) {
            var t = 0 | e;
            return t > n.options.borderWidth ? t - n.options.borderWidth / 2 : t
        }), n.offset = 20, n.animate = !1, t = n.createCanvas(), a(n, t)
    }
    var t;
    return function(e, t) {
        if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
        e.prototype = Object.create(t && t.prototype, {
            constructor: {
                value: e,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
    }(o, PIXI.Application), i(o, [{
        key: "createCanvas",
        value: (t = s(regeneratorRuntime.mark(function e() {
            var t;
            return regeneratorRuntime.wrap(function(e) {
                for (;;) switch (e.prev = e.next) {
                    case 0:
                        this.options.element.classList.add("noise-container"), this.view.classList.add("noise-canvas"), this.options.element.appendChild(this.view), this.container = new PIXI.Container, this.stage.addChild(this.container), e.next = 7;
                    case 7:
                        t = e.sent, this.waves = new PIXI.Sprite(t), this.noiseSprite = PIXI.Sprite.fromImage(this.options.displacementMap), this.setSize(), this.addGraphics(), this.bindEvents(), this.render(), $(this.options.element).hasClass("main__btn") || this.options.element.classList.add("canvas-ready");
                    case 15:
                    case "end":
                        return e.stop()
                }
            }, e, this)
        })), function() {
            return t.apply(this, arguments)
        })
    }, {
        key: "addGraphics",
        value: function() {
            this.container.addChild(this.getPolygon(!0)), this.options.waves && this.drawWaves();
            var e = new PIXI.Graphics;
            e.beginFill(0, 0), e.drawRect(0, 0, this.width, this.width), this.container.addChild(e), this.container.addChild(this.getPolygon()), this.setMask(), this.addFilter()
        }
    }, {
        key: "setMask",
        value: function() {
            var e = this.getPolygon();
            this.stage.addChild(e), this.container.mask = e
        }
    }, {
        key: "drawWaves",
        value: function() {
            this.waves.alpha = 1 - this.options.wavesAlpha, this.waves.y = this.height * this.options.wavesPos.y, this.container.addChild(this.waves), this.waves.width = this.waves.height = this.width
        }
    }, {
        key: "setSize",
        value: function() {
            var e = this.options.element.offsetWidth,
                t = this.options.element.offsetHeight;
            return this.width = e + 2 * this.offset, this.height = t + 2 * this.offset, this.oldWidth !== this.width && (this.renderer.resize(this.width, this.height), this.oldWidth = this.width, !0)
        }
    }, {
        key: "loadTexture",
        value: function(t) {
            return new Promise(function(i) {
                var e = new PIXI.loaders.Loader;
                e.add("waves", t), e.load(function(e, t) {
                    return i(t.waves.texture)
                })
            })
        }
    }, {
        key: "addFilter",
        value: function() {
            this.container.addChild(this.noiseSprite), this.noiseFilter = new PIXI.filters.DisplacementFilter(this.noiseSprite), this.container.filters = [this.noiseFilter], this.noiseSprite.position.x = -this.width, this.noiseSprite.width = 3 * this.width, this.noiseFilter.scale.x = 0, this.noiseFilter.scale.y = 0
        }
    }, {
        key: "createTimeLine",
        value: function() {
            var e = this;
            this.timeline = new TimelineMax({
                onUpdate: this.render.bind(this),
                paused: !0,
                onStart: function() {
                    return e.animate = !0
                },
                onComplete: function() {
                    return e.animate = !1
                }
            }).to(this.noiseFilter.scale, .2, {
                x: this.options.displacementScale.x,
                y: this.options.displacementScale.y
            }).fromTo(this.noiseSprite, .5, {
                x: -.66 * this.noiseSprite.width
            }, {
                x: 0
            }, "-=.2").to(this.noiseFilter.scale, .2, {
                x: 0,
                y: 0
            }, "-=.2")
        }
    }, {
        key: "play",
        value: function() {
            this.animate || this.timeline.play(0)
        }
    }, {
        key: "bindEvents",
        value: function() {
            this.createTimeLine(), this.options.element.addEventListener("mouseenter", this.play.bind(this))
        }
    }, {
        key: "getPolygon",
        value: function(e) {
            var t = this.polygon,
                i = new PIXI.Graphics,
                n = this.width - 2 * this.offset - this.options.borderWidth,
                o = this.height - 2 * this.offset - this.options.borderWidth;
            i.position.x = this.offset + this.options.borderWidth / 2, i.position.y = this.offset + this.options.borderWidth / 2;
            var s = [
                [0, t[0]],
                [t[0], 0],
                [n - t[1], 0],
                [n, t[1]],
                [n, o - t[2]],
                [n - t[2], o],
                [t[3], o],
                [0, o - t[3]],
                [0, t[0]]
            ];
            i.lineStyle(this.options.borderWidth, this.options.borderColor), i.beginFill(this.options.backgroundColor, e ? 1 - this.options.backgroundAlpha : 0);
            for (var a = 0, r = []; a < s.length; a++) r.length && r[0] === s[a][0] && r[1] === s[a][1] || (0 !== a ? (r = s[a], i.lineTo(s[a][0], s[a][1])) : (i.moveTo(s[a][0], s[a][1]), r = s[a]));
            return i.endFill(), i
        }
    }], [{
        key: "debounce",
        value: function(i, n, o, s) {
            var a = arguments,
                r = void 0;
            return function() {
                var e = a,
                    t = s && !r;
                clearTimeout(r), r = setTimeout(function() {
                    r = null, s || i.apply(n, e)
                }, o), t && i.apply(n, e)
            }
        }
    }]), o
}();

let rectangle_arrow = is_mobile() ? 15 : 23;
let borderWidth = is_mobile() ? 1 : 2;

// config btn add shadow
let config_btns = document.getElementsByClassName("scatter_config_btn");
for(let btn of config_btns){
  new e({
      element: btn,
      polygon: `${rectangle_arrow}, 0, ${rectangle_arrow}, 0`,
      wavesPos: {
          y: 0,
          x: 0
      },
      borderWidth,
      borderColor: "0x009dda",
      backgroundAlpha: 1,
      wavesAlpha: .8,
      waves: "../img/ex/ex1.jpg",
      displacementMap: displace_map
  });
}

new e({
    element: document.querySelector(".media_read_more"),
    polygon: `${rectangle_arrow}, 0, ${rectangle_arrow}, 0`,
    wavesPos: {
        y: 0,
        x: 0
    },
    borderWidth,
    borderColor: "0x009dda",
    backgroundAlpha: 1,
    wavesAlpha: .8,
    waves: "../img/ex/ex1.jpg",
    displacementMap: displace_map
});
new e({
    element: document.querySelector(".read_more2"),
    polygon: `${rectangle_arrow}, 0, ${rectangle_arrow}, 0`,
    wavesPos: {
        y: 0,
        x: 0
    },
    borderWidth,
    borderColor: "0x009dda",
    backgroundAlpha: 1,
    wavesAlpha: .8,
    waves: "../img/ex/ex1.jpg",
    displacementMap: displace_map
});

})();