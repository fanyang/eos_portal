import '../assets/css/index.css'
import '../assets/css/mobile.css'
import '../assets/images/wx-link.jpg'
import $ from 'jquery'
const call_lang_module = async (lang_key) => {
    let en = await import('../components/lang/' + lang_key + '.js');
    return en.default;
}
import '../assets/images/favicon.ico'
;(function(){
    $('[page_item]').eq(0).height(document.documentElement.clientHeight - 76);
    $('body').css({'opacity': 1});
})();
var lang = navigator.language || navigator.userLanguage;
lang = lang.substr(0, 2);
var key_map = {
    zh: 'cn',
    cn: 'cn',
    en: 'en',
    ko: 'kr',
    kr: 'kr',
}
var key = (window.location.href.split('lang=')[1]||'').toLowerCase()
key = key ? key : lang;
key = key_map[key] ? key_map[key] : 'en';
call_lang_module(key)
.then(lang => {
    key_map[key] = lang;
    $('[v-bind]').each(function(index, el){
    var attrs = $(el).attr('v-bind').split(';');
    $(attrs).each(function(i, v){
            var val = v.split(':');
            if(val[0] == 'text'){
                el.innerText = key_map[key][val[1]];
            }else if(val[0] == 'innerHTML'){
                el.innerHTML = key_map[key][val[1]];
            }else{
                $(el).attr(val[0], key_map[key][val[1]]);
            }
        })
    });

    // info_box_pane_start
    // $('#info_box_pane').removeClass('hide_box');

    // $('#close_info_box_pane').on('click', () => {
    //   $('#info_box_pane').addClass('hide_box');      
    // });
    // info_box_pane_end

    $('[view_more],.read_more').on('click', function() {
        let pt = $(this).parents('[view_more_ct]').eq(0);
        pt.addClass('view_more_ct');
        // $('[view_more]', pt).eq(0).hide();
        // $('[view_less]', pt).eq(0).show();
        $('[view_more]', pt).each(function(){
            if($(this).parents('[view_more_ct]').get(0) === pt.get(0)){
                $(this).eq(0).hide();        
            }
        });

        $('[view_less]', pt).each(function(){
            if($(this).parents('[view_more_ct]').get(0) === pt.get(0)){
                $(this).eq(0).show();        
            }
        });
    });
    // 
    $('[view_less]').on('click', function() {
        let pt = $(this).parents('[view_more_ct]').eq(0);
        pt.removeClass('view_more_ct');
        $('[view_more]', pt).each(function(){
            if($(this).parents('[view_more_ct]').get(0) === pt.get(0)){
                $(this).eq(0).show();        
            }
        });

        $('[view_less]', pt).each(function(){
            if($(this).parents('[view_more_ct]').get(0) === pt.get(0)){
                $(this).eq(0).hide();        
            }
        });
        
    });

    let page_items = $('[page_item]');
    let check_nav_top = () => {
        let body_top = document.documentElement.scrollTop || document.body.scrollTop;
        let win_height = document.documentElement.clientHeight;

        let pre = null, next, is_selected = false, mg_arr = [];
        page_items.each(function(index, el){
            if(is_selected){
                return ;
            }
            let e_top = $(el).offset().top;
            let e_down = e_top + el.offsetHeight;
            let mg_top = Math.abs(e_top - (body_top + win_height/2));
            let mg_bottom = Math.abs(e_down - (body_top + win_height/2));
            let min = Math.min(mg_top, mg_bottom);
            mg_arr.push(min);
        });
        let min_index = mg_arr.indexOf(Math.min(...mg_arr));
        $('[nv_item]').removeClass('nav_vertical_item_foucus');
        $('[nv_item]').eq(min_index).addClass('nav_vertical_item_foucus');
        if(body_top > win_height/2){
            expand ? expand() : null;
        }else{
            contract ? contract() : null;
        }
    }
    check_nav_top();
    $(document).on('scroll', function(){
        check_nav_top();
    });

    $('[nv_item]').on('click', function(){
        let index = $('[nv_item]').index(this);
        let index_pos = index ? $('[page_item]').eq(index).offset().top : 0;
        let ani_style = {
            'scrollTop': index_pos + 'px'
        }
        $(document.documentElement).animate(ani_style);
        $(document.body).animate(ani_style);
    });
    $('[to_page_item]').on('click', function(){
        let key = $(this).attr('to_page_item');
        let pane = $('[page_item="' + key + '"]');
        let index = page_items.index(pane);
        let index_pos = index ? $('[page_item]').eq(index).offset().top : 0;
        let ani_style = {
            'scrollTop': index_pos + 'px'
        }
        $(document.documentElement).animate(ani_style);
        $(document.body).animate(ani_style);
        $('[mobile_head_nav]').hide();
    });
    $('[nav_click="hide"]').on('click', function(){
        $('[mobile_head_nav]').hide();
    });
    $('[nav_click="show"]').on('click', function(){
        $('[mobile_head_nav]').show();
    });
    $('[mobile_head_nav]').on('click', function(e){
        let target = e.target || e.srcElement;
        if(target === this){
            $(this).hide();
        }
    });
});


import {expand, contract} from '../components/threejs/animate_actions.js'

